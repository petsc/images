# PETSc Images

For information on the usage of this repository in the PETSc documentation,
see the [PETSc Developers Documementation](https://petsc.org/release/developers/documentation#images)

This repository serves a secondary purpose as a natural place to look for current
images, diagrams, logos, etc. related to PETSc, when making a presentation or report.
