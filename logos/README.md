# PETSc Logos and Icons

## Font
  * Professional logos: Proxima Nova
  * Open source replacement: [Lexend](https://fonts.google.com/specimen/Lexend)

When working with SVG files, convert text to paths for portability.

## RGB Hex codes for colors

```
404243 Dark grey
bacc33 Green
bc3b3a Red
6ca39e Teal
d86035 Orange
305e89 Blue
```